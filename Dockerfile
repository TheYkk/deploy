FROM alpine:3.12

LABEL maintainer="Kaan Karakaya <yusufkaan142@gmail.com>"

ENV KUBE_LATEST_VERSION="v1.18.9"

RUN apk add --update ca-certificates \
 && apk add --update -t deps curl openssl bash \
 && curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
 && chmod +x /usr/local/bin/kubectl

RUN mkdir -p $HOME/.kube

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 \
 && chmod 700 get_helm.sh \
 && ./get_helm.sh

RUN apk del --purge deps \
 && rm /var/cache/apk/*
WORKDIR /root
